<?php
$currency = [
    'uah' => [
        'name' => 'Гривна',
        'course' => 1
    ],
    'usd' => [
        'name' => 'Доллар',
        'course' => 27.1
    ],
    'eur' => [
        'name' => 'Евро',
        'course' => 30.2
    ] 
    ];
$goods = [
        [
            'title' => 'Кальмар щупальца',
            'price_val' => 435,
            'discount_type' => 'value',
            'discount_val' => 18
        ],
        [
            'title' => 'Мясо гребешков',
            'price_val' => 355.55,
            'discount_type' => 'percent',
            'discount_val' => 25
        ],
        [
            'title' => 'Мидии черные целые в ракушке',
            'price_val' => 165,
            'discount_type' => 'percent',
            'discount_val' => 10
        ],
        [
            'title' => 'Осьминог сырой целый',
            'price_val' => 585.7,
            'discount_type' => 'percent',
            'discount_val' => 5
        ],
        [
            'title' => 'Окунь красный морской',
            'price_val' => 180,
            'discount_type' => 'value',
            'discount_val' => 30
        ],
        [
            'title' => 'Хек тушка без головы',
            'price_val' => 82.36,
            'discount_type' => 'percent',
            'discount_val' => 3.5
        ],
        [
            'title' => 'Палтус тушка',
            'price_val' => 395,
            'discount_type' => 'percent',
            'discount_val' => 50
        ],
        [
            'title' => 'Лосось филе',
            'price_val' => 470.85,
            'discount_type' => 'percent',
            'discount_val' => 8
        ],
        [
            'title' => 'Сардины в растительном масле',
            'price_val' => 38,
            'discount_type' => 'value',
            'discount_val' => 12
        ],
        [
            'title' => 'Креветка тигровая сырая',
            'price_val' => 330,
            'discount_type' => 'value',
            'discount_val' => 27
        ]
    ];
function getPriceWithDiscount($uahPrice, $typeDiscount, $valueDiscount){ // Функция вычисления стоимости с учётом типа скидки в гривне
    switch ($typeDiscount) {
        case 'value':
            return round($uahPrice - $valueDiscount, 2);
            break;
        case 'percent':
            return round($uahPrice - $uahPrice * $valueDiscount / 100, 2);
            break;
            
        /*default:
            echo 'Неверно указан тип скидки!';
            break;*/
    }
}

function convertPrice($discountResult, $currencyRate){ // Функция конвертации в выбранную валюту с учётом скидки
    switch ($currencyRate) {
        case 27.1:
            return round($discountResult / $currencyRate, 2) . ' usd';
            break;
        case 30.2:
            return round($discountResult / $currencyRate, 2) . ' eur';
            break;
        case 1:
            return round($discountResult / $currencyRate, 2) . ' uah';
            break;
    }
}

function convertWithoutDiscount($uahPrice, $currencyRate){ // Функция конвертации стоимости в выбранную валюту без скидки
    switch ($currencyRate) {
        case 27.1:
            return round($uahPrice / $currencyRate, 2) . ' usd';
            break;
        case 30.2:
            return round($uahPrice / $currencyRate, 2) . ' eur';
            break;
        case 1:
            return round($uahPrice / $currencyRate, 2) . ' uah';
            break;
    }
}

function showDiscountTypeValue($typeDiscount, $valueDiscount){ //Функция для отображения типа скидки и её значения
    switch ($typeDiscount) {
        case 'value':
            return 'абсолютная / ' . $valueDiscount . ' uah';
            break;
        case 'percent':
            return 'относительная / ' . $valueDiscount . ' %';
            break;
    }
}
?>

    