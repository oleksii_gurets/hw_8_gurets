<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] . '/functionality.php'; //Подключаем файл с массивами и функциями
if(!empty($_SESSION['data'])){
    $choice = (float)$_SESSION['data'];
}else{
    $choice = $currency['uah']['course'];
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Домашнее задание №8 Гурца Алексея</title>
    <meta name="description" content="Сессии и функции">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>

<body>
    <div class="container-fluid">
        <header class="p-3 m-3 text-warning" style="background-color: #4b6477;">
            <div class="text-center">
                <h1>Домашнее задание №8</h1>
                <h2>Работа с функциями. Вывод данных в таблицы при помощи функций с сохранением в сессию</h2>
            </div>
        </header>
    </div>
    <div class="container mb-2">
        <div class="row">
            <form action="currency_choice.php" method="POST">
                <div class="mb-3">
                    <label>
                        <strong>Выбор валюты</strong>
                        <select class="form-select bg-secondary text-white" name="choice_value">
                            <option selected disabled>
                                <?php switch($_SESSION['data']): case $currency['usd']['course']:
                                    echo 'Выбранная валюта: ' . $currency['usd']['name'];
                                    break;
                                case $currency['eur']['course']:
                                    echo 'Выбранная валюта: ' . $currency['eur']['name'];
                                    break;
                                case $currency['uah']['course']:
                                    echo 'Выбранная валюта: ' . $currency['uah']['name'];
                                    break;
                                default:
                                    echo 'Валюта по умолчанию: ' . $currency['uah']['name'];
                                endswitch; ?>
                            </option>
                            <option value="<?=$currency['usd']['course'];?>">USD</option>
                            <option value="<?=$currency['eur']['course'];?>">EUR</option>
                            <option value="<?=$currency['uah']['course'];?>">UAH</option>
                        </select>
                    </label>
                </div>
                <button type="submit" class="btn btn-dark">Принять</button>
            </form>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <form action="delete_choice.php" method="POST">
                <div class="mb-3">
                    <input type="hidden" name="deldata">
                    <button class="btn btn-danger">Сброс данных</button>
                </div>
            </form>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <table class="table table-hover table-bordered border-primary">
                <thead class="table-primary border-primary">
                    <tr style="text-align: center;">
                        <th scope="col">Наименование товара</th>
                        <th scope="col">Изначальная цена в выбранной валюте</th>
                        <th scope="col">Тип скидки/значение скидки</th>
                        <th scope="col">Цена по скидке</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($goods as $item): 
                        /*Вывод таблицы по массиву, в функции передаем в качестве аргументов значения из массивов и переменную $choice*/?>
                        <tr style="text-align: center;">
                            <td class="table-secondary border-primary"><?=$item['title'];?></td>
                            <td class="table-success border-primary"><?=convertWithoutDiscount($item['price_val'], $choice); ?></td>
                            <td class="table-primary border-primary"><?=showDiscountTypeValue($item['discount_type'], $item['discount_val']);?></td>
                            <td class="table-secondary border-primary">
                                <?=convertPrice(getPriceWithDiscount($item['price_val'], $item['discount_type'], $item['discount_val']), $choice);
                                // В качестве первого аргумента передаем функцию getPriceWithDiscount с аргументами из массивов?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="container-fluid">
        <footer class="d-flex justify-content-center p-1 m-1 text-warning" style="background-color: #4b6477;">
            <p>Гурец Алексей &copy;2021 <a href="mailto:oleksii.gurets@gmail.com" class="text-warning">Все вопросы по
                    почте</a> <a href="/" class="text-warning">Главная</a></p>
        </footer>
    </div>
    
</body>

</html>